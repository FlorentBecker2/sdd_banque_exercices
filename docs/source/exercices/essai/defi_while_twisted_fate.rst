Twisted Fate
-------------
Twisted Fate s'ennuie... Pour passer le temps, il décide de construire des châteaux de cartes.
* Il lui faut 15 cartes pour construire un chateau de 3 étages!
  

* Il lui faut 26 cartes pour construire un chateau de 4 étages!


Il vous demande d'écrire une fonction qui prendra en paramètre le nombre maximum de cartes que Twisted Fate veut utiliser, et qui déterminera le nombre d'étage du plus haut château qu'il peut construire. Par exemple, avec 23 cartes, le résultat attendu est 3 (il y a assez de cartes pour construire un château de 3 étages, mais pas assez pour un château de 4 étages)



.. easypython:: twisted_fate.py
   :language: python
   :uuid: 1231313
