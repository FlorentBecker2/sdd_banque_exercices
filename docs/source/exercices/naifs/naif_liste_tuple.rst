Fonction sur les listes
-----------------------

On veut écrire une fonction qui prend en paramètre une liste non vide de nombres
et qui renvoie le maximum de cette liste ainsi que sa position.

1. Quelle structure de données allons-nous utiliser pour le résultat de cette fonction ?

2. Donnez le code d'une telle fonction.
