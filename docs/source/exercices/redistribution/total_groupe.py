from hypothesis import given, example
import hypothesis.strategies as st
from fail_test import fail_test

attendu_etudiant = ["total_groupe"]


def close_enough(f1, f2, tolerance):
   if f1 == f2:
      return True # pour 0 et +/- infini
   a1 = abs(f1)
   a2 = abs(f2)
   return (abs(f1 - f2) / max(a1, a2)) < tolerance


@solution
def total_groupe(g):
    return sum(sum(x) for x in g.values())

@given(st.dictionaries(st.text(min_size=5),
                       st.lists(st.floats(allow_nan=False, allow_infinity=False))))
@example({ 'Dominique': [15, 12, 8, 3],
           'Claude' : [20, 34],
	   'Mérédith' : [52],
	   'Alex' : [8]})
def test_total_est_sum_sum(code_etu, s):
    mon_total = sum(sum(l) for l in s.values())
    son_total = code_etu.total_groupe(s)
    if not close_enough(mon_total, son_total, 0.00001):
        fail_test("Sur l'entrée %r, vous retournez %r au lieu de %r" % (s, son_total, mon_total))

